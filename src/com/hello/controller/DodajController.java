package com.hello.controller;

import com.hello.form.AuticForm;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;
import java.util.Map;

@Controller

public class DodajController {

    @RequestMapping(value = "/dodajcar", method = RequestMethod.GET)
    public String viewRegistration(Map<String, Object> model) {
      AuticForm dodavanjeAuta = new AuticForm();
      model.put("dodavanjeAuta", dodavanjeAuta);
        return "dodaj";
    }

    @RequestMapping(value = "/processdodajcar", method = RequestMethod.POST)
    public String processRegistration(@Valid @ModelAttribute("dodavanjeAuta") AuticForm auticForm, BindingResult bindingResult,
                                      Map<String, Object> model) {
        if (bindingResult.hasErrors()) {
            return "dodaj";
        }
        else {
            System.out.println("Sifra: " + auticForm.getSifra());
            System.out.println("Naziv: " + auticForm.getNaziv());
            System.out.println("Godiste: " + auticForm.getGodiste());
            System.out.println("Cena: " + auticForm.getCena());
            System.out.println("Broj vrata: " + auticForm.getBrojVrata());
            System.out.println("Boja: " + auticForm.getBoja());
            System.out.println("Tip motora: " + auticForm.getTipMotora());
            System.out.println("Tip menjaca: " + auticForm.getMenjac());
            System.out.println("Presao: " + auticForm.getPresao());
            System.out.println("Registrovan: " + auticForm.isRegistrovan());

            return "prikazidodato";
        }
    }
}