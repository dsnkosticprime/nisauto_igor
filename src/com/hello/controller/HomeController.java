package com.hello.controller;

import com.hello.service.AutomobileService;
import com.hello.DTO.BasicAutoDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;


@Controller
public class HomeController {

    @Autowired
    private AutomobileService automobileService;


    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String getData(Model model) {

        List<BasicAutoDTO> katalog = automobileService.getList();

        model.addAttribute("katalog", katalog);

        return "home";
    }
}
