package com.hello.controller;

import com.hello.service.AutomobileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;


@Controller
public class OglasController {

    @Autowired
    private AutomobileService automobileService;

    @RequestMapping(value = "/prikazicar", method = RequestMethod.GET, params = {"sifra"})
    public String getCarByFigure(
        @RequestParam("sifra") int sifra, Model model) {
        model.addAttribute("car", automobileService.getCar(sifra));
        return "prikazioglas";
    }
}


