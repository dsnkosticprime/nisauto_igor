package com.hello.DAO;

import com.hello.model.Automobile;

import java.util.ArrayList;
import java.util.List;

public class AutomobileMemoryDAO {

    private List<Automobile> listaAutomobila = new ArrayList<Automobile>();

    public AutomobileMemoryDAO (){
        listaAutomobila.add(new Automobile(5005,"Audi TT", 2002, 7500.00, 5, "crna", "benzin", "automatski", 150000.00, true));
        listaAutomobila.add(new Automobile(1423,"Fiat 500 1.2", 2015, 5000.00, 5, "crna", "benzin", "automatski", 150000.00, true));
        listaAutomobila.add(new Automobile(2874,"Yugo 55", 1988, 550.00, 5, "crna", "benzin", "automatski", 150000.00, true));
        listaAutomobila.add(new Automobile(8007,"Porsche 911 Carrera 4", 2013, 75000.00, 5, "crna", "benzin", "automatski", 150000.00, true));
    }

    public List<Automobile> getList() {

        return listaAutomobila;
    }

    public Automobile prikaziOglas (int sifra){

        Automobile auto = new Automobile();

         for (int i = 0; i < listaAutomobila.size(); i++) {
             if (listaAutomobila.get(i).getSifra()==sifra)
                auto = listaAutomobila.get(i);
            }
        return auto;
    }

}


