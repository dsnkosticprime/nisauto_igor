package com.hello.DTO;

import com.hello.model.Automobile;

public class BasicAutoDTO {
    private int sifra;
    private String naziv;
    private int godiste;
    private double cena;

    public BasicAutoDTO(Automobile automobile) {
        sifra = automobile.getSifra();
        naziv = automobile.getNaziv();
        godiste = automobile.getGodiste();
        cena = automobile.getCena();
    }

    public int getSifra() {
        return sifra;
    }

    public String getNaziv() {
        return naziv;
    }

    public int getGodiste() {
        return godiste;
    }

    public double getCena() {
        return cena;
    }
}
