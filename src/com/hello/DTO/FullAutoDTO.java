package com.hello.DTO;

import com.hello.model.Automobile;

public class FullAutoDTO {
    private int sifra;
    private String naziv;
    private int godiste;
    private double cena;
    private int brojVrata;
    private String boja;
    private String tipMotora;
    private String menjac; // false manuelni, true automatski
    private double presao;
    private boolean registrovan;

    public FullAutoDTO(Automobile automobile) {
        this.sifra = automobile.getSifra();
        this.naziv = automobile.getNaziv();
        this.godiste = automobile.getGodiste();
        this.cena = automobile.getCena();
        this.brojVrata = automobile.getBrojVrata();
        this.boja = automobile.getBoja();
        this.tipMotora = automobile.getTipMotora();
        this.menjac = automobile.getMenjac();
        this.presao = automobile.getPresao();
        this.registrovan = automobile.isRegistrovan();
    }

    public int getSifra() {
        return sifra;
    }

    public String getNaziv() {
        return naziv;
    }

    public int getGodiste() {
        return godiste;
    }

    public double getCena() {
        return cena;
    }

    public int getBrojVrata() {
        return brojVrata;
    }

    public String getBoja() {
        return boja;
    }

    public String getTipMotora() {
        return tipMotora;
    }

    public String getMenjac() {
        return menjac;
    }

    public double getPresao() {
        return presao;
    }

    public boolean isRegistrovan() {
        return registrovan;
    }
}
