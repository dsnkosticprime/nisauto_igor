package com.hello.model;

public class Automobile {

    private int sifra;
    private String naziv;
    private int godiste;
    private double cena;
    private int brojVrata;
    private String boja;
    private String tipMotora;
    private String menjac; // false manuelni, true automatski
    private double presao;
    private boolean registrovan;

    public Automobile(){

    }

    public Automobile(int sifra, String naziv, int godiste, double cena, int brojVrata, String boja, String tipMotora, String menjac, double presao, boolean registrovan) {
        this.sifra = sifra;
        this.naziv = naziv;
        this.godiste = godiste;
        this.cena = cena;
        this.brojVrata = brojVrata;
        this.boja = boja;
        this.tipMotora = tipMotora;
        this.menjac = menjac;
        this.presao = presao;
        this.registrovan = registrovan;
    }

    @Override
    public String toString() {
        return  "Sifra=" + sifra +
                ", Naziv='" + naziv + '\'' +
                ", Godiste=" + godiste +
                ", Cena=" + cena +
                ", Broj Vrata=" + brojVrata +
                ", Boja='" + boja + '\'' +
                ", Tip Motora='" + tipMotora + '\'' +
                ", Menjac =" + menjac +
                ", Presao (KM)=" + presao +
                ", Registrovan (false - NE, true - DA)=" + registrovan +
                '}';
    }

    public int getSifra() {
        return sifra;
    }

    public void setSifra(int sifra) {
        this.sifra = sifra;
    }

    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public int getGodiste() {
        return godiste;
    }

    public void setGodiste(int godiste) {
        this.godiste = godiste;
    }

    public double getCena() {
        return cena;
    }

    public void setCena(double cena) {
        this.cena = cena;
    }

    public int getBrojVrata() {
        return brojVrata;
    }

    public void setBrojVrata(int brojVrata) {
        this.brojVrata = brojVrata;
    }

    public String getBoja() {
        return boja;
    }

    public void setBoja(String boja) {
        this.boja = boja;
    }

    public String getTipMotora() {
        return tipMotora;
    }

    public void setTipMotora(String tipMotora) {
        this.tipMotora = tipMotora;
    }

    public String getMenjac() {
        return menjac;
    }

    public void setMenjac(String menjac) {
        this.menjac = menjac;
    }

    public double getPresao() {
        return presao;
    }

    public void setPresao(double presao) {
        this.presao = presao;
    }

    public boolean isRegistrovan() {
        return registrovan;
    }

    public void setRegistrovan(boolean registrovan) {
        this.registrovan = registrovan;
    }
}
