package com.hello.form;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;
import javax.validation.constraints.*;


public class AuticForm {

    @NotNull
    @Min(1000)
    @Max(9999)
    private Integer sifra;

    @NotEmpty
    @Length(min=3, max=60)
    private String naziv;

    @Min(1850)
    @Max(2150)
    private Integer godiste;


    @NotNull
    private Double cena;

    private Integer brojVrata;
    private String boja;
    private String tipMotora;
    private String menjac;
    private Double presao;
    private boolean registrovan;

    public Integer getSifra() {
        return sifra;
    }

    public void setSifra(Integer sifra) {
        this.sifra = sifra;
    }

    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public Integer getGodiste() {
        return godiste;
    }

    public void setGodiste(Integer godiste) {
        this.godiste = godiste;
    }

    public Double getCena() {
        return cena;
    }

    public void setCena(Double cena) {
        this.cena = cena;
    }

    public Integer getBrojVrata() {
        return brojVrata;
    }

    public void setBrojVrata(Integer brojVrata) {
        this.brojVrata = brojVrata;
    }

    public String getBoja() {
        return boja;
    }

    public void setBoja(String boja) {
        this.boja = boja;
    }

    public String getTipMotora() {
        return tipMotora;
    }

    public void setTipMotora(String tipMotora) {
        this.tipMotora = tipMotora;
    }

    public String getMenjac() {
        return menjac;
    }

    public void setMenjac(String menjac) {
        this.menjac = menjac;
    }

    public Double getPresao() {
        return presao;
    }

    public void setPresao(Double presao) {
        this.presao = presao;
    }

    public boolean isRegistrovan() {
        return registrovan;
    }

    public void setRegistrovan(boolean registrovan) {
        this.registrovan = registrovan;
    }
}


