package com.hello.service;

import com.hello.model.Automobile;
import com.hello.DAO.AutomobileMemoryDAO;
import com.hello.DTO.BasicAutoDTO;
import com.hello.DTO.FullAutoDTO;
import org.springframework.beans.factory.annotation.Required;

import java.util.ArrayList;
import java.util.List;


public class AutomobileService {
    private AutomobileMemoryDAO automobileMemoryDAO;

    public AutomobileService(){}

    public AutomobileService(AutomobileMemoryDAO automobileMemoryDAO){
        this.automobileMemoryDAO = automobileMemoryDAO;
    }

    public List<BasicAutoDTO> getList() {
        List<Automobile> transferlista = automobileMemoryDAO.getList();
        List<BasicAutoDTO> katalog = new ArrayList<BasicAutoDTO>();

        for (int i = 0; i < transferlista.size(); i++) {
          Automobile auto = transferlista.get(i);
                katalog.add(new BasicAutoDTO(auto));
        }
        return katalog;
    }


    public FullAutoDTO getCar (int sifra){
        Automobile auto = automobileMemoryDAO.prikaziOglas(sifra);
        FullAutoDTO skraceno = new FullAutoDTO(auto);
        return skraceno;
    }

    @Required
    public void setAutomobileMemoryDAO(AutomobileMemoryDAO AutomobileMemoryDAO) {
        automobileMemoryDAO = AutomobileMemoryDAO;
    }

    public AutomobileMemoryDAO getAutomobileMemoryDAO() {
        return automobileMemoryDAO;
    }
}
