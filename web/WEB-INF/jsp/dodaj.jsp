<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Dodavanje novog automobila</title>

    <style>
        .error {
            color: #ff0000;
        }

        .errorblock {
            color: #000;
            background-color: #ffEEEE;
            border: 3px solid #ff0000;
            padding: 8px;
            margin: 16px;
        }
    </style>
</head>
<body>
<div align="center">
    <form:form action="processdodajcar" method="post" commandName="dodavanjeAuta">
    <form:errors path = "*" cssClass = "errorblock" element = "div" />
        <table border="0">
            <tr>
                <td colspan="2" align="center"><h2>Dodavanje novog automobila</h2></td>
            </tr>

            <tr>
                <td>Unesite sifru automobila:</td>
                <td><form:input path="sifra" /></td>
                <td><form:errors path="sifra" cssClass = "error"/></td>

            </tr>
            <tr>
                <td>Unesite naziv automobila:</td>
                <td><form:input path="naziv" /></td>
                <td><form:errors path="naziv" cssClass = "error"/></td>
            </tr>
            <tr>
                <td>Unesite godinju proizvodnje automobila:</td>
                <td><form:input path="godiste" /></td>
                <td><form:errors path="godiste" cssClass = "error"/></td>
            </tr>
            <tr>
                <td>Unesite cenu automobila</td>
                <td><form:input path="cena" /></td>
                <td><form:errors path="cena" cssClass = "error"/></td>
            </tr>
            <tr>
                <td>Unesite broj vrata: </td>
                <td><form:input path="brojVrata" /></td>
                <td><form:errors path="brojVrata" cssClass = "error"/></td>
            </tr>
            <tr>
                <td>Unesite boju automobila:</td>
                <td><form:input path="boja" /></td>
                <td><form:errors path="boja" cssClass = "error"/></td>
            </tr>
            <tr>
                <td>Unesite tip motora:</td>
                <td><form:input path="tipMotora" /></td>
                <td><form:errors path="tipMotora" cssClass = "error"/></td>
            </tr>
            <tr>
                <td>Unesite tip menjaca:</td>
                <td><form:input path="menjac" /></td>
                <td><form:errors path="menjac" cssClass = "error"/></td>
            </tr>
            <tr>
                <td>Unesite predjenu kilometrazu automobila</td>
                <td><form:input path="presao" /></td>
                <td><form:errors path="presao" cssClass = "error"/></td>
            </tr>
            <tr>
                <td>Unesite da li je automobil registrovan: </td>
                <td><form:checkbox path="registrovan" /></td>
                <td><form:errors path="registrovan" cssClass = "error"/></td>
            </tr>
            <tr>
                <td colspan="2" align="center"><input type="submit" value="Dodaj" /></td>
            </tr>

        </table>
    </form:form>
</div>
</body>
</html>