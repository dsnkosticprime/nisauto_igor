<%@ page contentType = "text/html; charset = UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<html>
<head>
    <link href="styles.css" rel="stylesheet">
    <title>NA-004 Izgled home stranice</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css"/>

    <style>
        .button {
            background-color: #4CAF50; /* Green */
            border: none;
            color: white;
            padding: 16px 32px;
            text-align: center;
            text-decoration: none;
            display: inline-block;
            font-size: 16px;
            margin: 4px 2px;
            -webkit-transition-duration: 0.4s; /* Safari */
            transition-duration: 0.4s;
            cursor: pointer;
        }

        .button1 {
            background-color: white;
            color: black;
            border: 2px solid #4CAF50;

        }

        .button1:hover {
            background-color: #4CAF50;
            color: white;
        }

        .wrapper {
            text-align: center;
        }

    </style>
</head>
<body>

<%--<h2>Lista automobila</h2>--%>

<%--<c:forEach items="${lists}" var="object">--%>
<%--${object}--%>
<%--<br>--%>
<%--</c:forEach>--%>
<div class="header">
    <h1 align="center"> <img src="../../resources/car.png" height="64" width="64"/>NA-004 Prikaz oglasa</h1>
</div>

<div class="container">
    <br>
    <div class="table-responsive">
        <table class="table">
            <tr class="active"><th style="text-align:center">Le Chiffre
                <th style="text-align:center">Naziv </th>
                <th style="text-align:center">Godiste </th>
                <th style="text-align:center">Cena </th>
                <th style="text-align:center">Broj Vrata </th>
                <th style="text-align:center">Boja </th>
                <th style="text-align:center">Tip Motora </th>
                <th style="text-align:center">Menjac </th>
                <th style="text-align:center">Presao </th>
                <th style="text-align:center">Registrovan </th>
                </tr>

                <tr class="active">
                    <td style="text-align:center">${car.sifra}</td>
                    <td style="text-align:center">${car.naziv}</td>
                    <td style="text-align:center">${car.godiste}</td>
                    <td style="text-align:center">${car.cena}</td>
                    <td style="text-align:center">${car.brojVrata}</td>
                    <td style="text-align:center">${car.boja}</td>
                    <td style="text-align:center">${car.tipMotora}</td>
                    <td style="text-align:center">${car.menjac}</td>
                    <td style="text-align:center">${car.presao}</td>
                    <td style="text-align:center">${car.registrovan}</td>
                </tr>
        </table>
    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    <div class="wrapper">
        <a href="${pageContext.servletContext.contextPath}/">
        <button class="button button1">POVRATAK NA GLAVNU STRANU</button>
        </a>
    </div>

</div>
</body>
</html>