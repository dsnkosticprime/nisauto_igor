<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Automobil uspesno dodat!</title>
</head>
<body>
<div align="center">
    <table border="0">
        <tr>
            <td colspan="2" align="center"><h2>Automobil uspesno dodat!</h2></td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <h3>Hvala na unosu! Ovo su informacije o automobilu koji ste uneli:</h3>
            </td>
        </tr>
        <tr>
            <td>Sifra:</td>
            <td>${dodavanjeAuta.sifra}</td>
        </tr>
        <tr>
            <td>Naziv:</td>
            <td>${dodavanjeAuta.naziv}</td>
        </tr>
        <tr>
            <td>Godina proizvodnje:</td>
            <td>${dodavanjeAuta.godiste}</td>
        </tr>
        <tr>
            <td>Cena:</td>
            <td>${dodavanjeAuta.cena}</td>
        </tr>
        <tr>
            <td>Broj vrata:</td>
            <td>${dodavanjeAuta.brojVrata}</td>
        </tr>
        <tr>
            <td>Boja:</td>
            <td>${dodavanjeAuta.boja}</td>
        </tr>
        <tr>
            <td>Tip motora:</td>
            <td>${dodavanjeAuta.tipMotora}</td>
        </tr>
        <tr>
            <td>Tip menjaca:</td>
            <td>${dodavanjeAuta.menjac}</td>
        </tr>
        <tr>
            <td>Predjena kilometraza:</td>
            <td>${dodavanjeAuta.presao}</td>
        </tr>
        <tr>
            <td>Registrovan:</td>
            <td>${dodavanjeAuta.registrovan}</td>
        </tr>

    </table>
</div>
</body>
</html>