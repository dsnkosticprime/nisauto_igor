<%@ page contentType = "text/html; charset = UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<html>
<head>
    <link href="styles.css" rel="stylesheet">
    <title>NA-004 Izgled home stranice</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css"/>

    <style>
        .button {
            background-color: #4CAF50; /* Green */
            border: none;
            color: white;
            padding: 16px 32px;
            text-align: center;
            text-decoration: none;
            display: inline-block;
            font-size: 16px;
            margin: 4px 2px;
            -webkit-transition-duration: 0.4s; /* Safari */
            transition-duration: 0.4s;
            cursor: pointer;
        }

        .button1 {
            background-color: white;
            color: black;
            border: 2px solid #4CAF50;
        }

        .button1:hover {
            background-color: #4CAF50;
            color: white;
        }

    </style>
</head>
<body>

<%--<h2>Lista automobila</h2>--%>

<%--<c:forEach items="${lists}" var="object">--%>
    <%--${object}--%>
    <%--<br>--%>
<%--</c:forEach>--%>
<div class="header">

    <h1 align="center"> <img src="../../resources/car.png" height="64" width="64"/> NA-004 Izgled home stranice</h1>
</div>

<div class="container">

    <br>


    <div class="table-responsive">

        <table class="table">
            <tr class="active"><th style="text-align:center">Le Chiffre
                <img src="../../resources/up-arrow.png" height="18" width="18" align="middle"/>
                <img src="../../resources/down-arrow.png" height="18" width="18" align="middle"/></th>
                <th style="text-align:center">Naziv
                    <img src="../../resources/up-arrow.png" height="18" width="18" align="middle"/>
                    <img src="../../resources/down-arrow.png" height="18" width="18" align="middle"/></th>
                <th style="text-align:center">Godiste
                    <img src="../../resources/up-arrow.png" height="18" width="18" align="middle"/>
                    <img src="../../resources/down-arrow.png" height="18" width="18" align="middle"/></th>
                <th style="text-align:center">Cena
                    <img src="../../resources/up-arrow.png" height="18" width="18" align="middle"/>
                    <img src="../../resources/down-arrow.png" height="18" width="18" align="middle"/></th>
                <th style="text-align:center">Akcije</th></tr>

            <c:forEach items="${katalog}" var="obj">
              <tr class="active">
                  <td style="text-align:center">${obj.sifra}</td>
                  <td style="text-align:center"><a href="${pageContext.servletContext.contextPath}/prikazicar?sifra=${obj.sifra}">
                          ${obj.naziv}</td>  </a>
                  <td style="text-align:center">${obj.godiste}</td>
                  <td style="text-align:center">${obj.cena}</td>
                  <td align="center">
                  <a href="${pageContext.servletContext.contextPath}/prikazicar?sifra=${obj.sifra}">
                  <img src="../../resources/information.png" height="24" width="24" align="middle"/>
                  </a>
                  <img src="../../resources/edit.png" height="24" width="24" align="middle"/>
                  <img src="../../resources/delete.png" height="24" width="24" align="middle"/></td>
              </tr>
            </c:forEach>

        </table>
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    <a href="${pageContext.servletContext.contextPath}/dodajcar">
    <button class="button button1">
       DODAJ OGLAS</button></a>
</div>
</body>
</html>